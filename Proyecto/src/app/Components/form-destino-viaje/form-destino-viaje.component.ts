import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { DestinoViaje } from '../../models/DestinoViaje.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { APP_CONFIG, AppConfig } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() itemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLong = 5;
  searchResults: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.itemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.NombreValidador,
        this.NombreValidadorParamtrizable(this.minLong)
      ])],
      url: ['']
    });
  }

  ngOnInit(): void {
    const elemNombre = document.getElementById('nombre') as HTMLInputElement;
    fromEvent(elemNombre, 'input').pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 3),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((text: string) => ajax(this.config.apiEndPoint + '/ciudades?q=' + text))
    ).subscribe(ajaxResponse => this.searchResults = AjaxResponse.response);
  }

  Guardar(nombre: string, url: string): boolean {
    const d = new DestinoViaje(nombre, url);
    this.itemAdded.emit(d);
    return false;
  }

  NombreValidador(control: FormControl): { [s: string]: boolean } {
    const l = control.value.toString().trim().length;
    if (l >= 0 && l < 5) {
      return { invalidNombre: true };
    }
    return null;
  }

  NombreValidadorParamtrizable(minLong: number): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean } | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) {
        return { minLongNombre: true };
      }
      return null;
    };
  }
}
