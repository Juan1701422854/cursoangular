import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './Components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './Components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './Components/destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './Components/form-destino-viaje/form-destino-viaje.component';
import { DestinosApiClient } from './models/DestinosApiClient.model';
import { DestinosViajesState, reducerDestinosViajes, initializeDestinosViajesState, DestinoViajesEffects } from './models/DestinoViajesState.model';
import { LoginComponent } from './Components/login/login/login.component';
import { ProtectedComponent } from './Components/protected/protected/protected.component';
import { UsuarioLoggeadoGuard } from './guards/usuario-loggeado/usuario-loggeado.guard';
import { AuthService } from './services/auth.service';
import { VuelosComponentComponent } from './Components/vuelos/vuelos/vueloscomponent';
import { VuelosMainComponentComponent } from './Components/vuelos/vuelos-main/vuelos-main.component';
import { VuelosMasInfoComponentComponent } from './Components/vuelos/vuelos-mas-info/vuelos-mas-info.component';
import { VuelosDetalleComponentComponent } from './Components/vuelos/vuelos-detalle/vuelos-detalle.component';
import { ReservasModule } from './reservas/reservas.module';

export interface AppConfig {
  apiEndPoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndPoint: 'https://localhost:3000'
};

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

export const childrenRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: VuelosMainComponentComponent},
  { path: 'mas-info', component: VuelosMasInfoComponentComponent},
  { path: ':id', component: VuelosDetalleComponentComponent }
];

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ListaDestinosComponent },
  { path: 'destino/:id', component: DestinoDetalleComponent },
  { path: 'login', component: LoginComponent },
  { path: 'protected',
    component: ProtectedComponent,
    canActivate: [ UsuarioLoggeadoGuard ]
  },
  {
    path: 'vuelos',
    component: VuelosComponentComponent,
    canActivate: [ UsuarioLoggeadoGuard ],
    children: childrenRoutesVuelos
  }
];

export interface AppState {
  destinos: DestinosViajesState;
}

const reducers: ActionReducerMap<AppState> = {
  destinos: reducerDestinosViajes
};

const reducersInitialState = {
  destinos: initializeDestinosViajesState()
};

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponentComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState }),
    EffectsModule.forRoot([DestinoViajesEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule
  ],
  providers: [
    AuthService, UsuarioLoggeadoGuard,
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
