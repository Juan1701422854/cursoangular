import { Injectable } from '@angular/core';
import { DestinoViaje } from './DestinoViaje.model';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

export interface DestinosViajesState {
  items: DestinoViaje[];
  loading: boolean;
  favorito: DestinoViaje;
}

// tslint:disable-next-line: only-arrow-functions
export const initializeDestinosViajesState = function() {
  return {
    items: [],
    loading: false,
    favorito: null
  };
};

export enum DestinosViajesActionTypes {
  nuevoDestino = '[Destinos Viajes] Nuevo',
  elegidoFavorito = '[Destinos Viajes] Favorito',
  voteUp = '[Destinos Viajes] Vote Up',
  voteDown = '[Destinos Viajes] Vote Down'
}

export class NuevoDestinoAction implements Action {
  type = DestinosViajesActionTypes.nuevoDestino;
  constructor(public destino: DestinoViaje) {}
}

export class ElegidoFavoritoAction implements Action {
  type = DestinosViajesActionTypes.elegidoFavorito;
  constructor(public destino: DestinoViaje) {}
}

export class VoteUpAction implements Action {
  type = DestinosViajesActionTypes.voteUp;
  constructor(public destino: DestinoViaje) {}
}

export class VoteDownAction implements Action {
  type = DestinosViajesActionTypes.voteDown;
  constructor(public destino: DestinoViaje) {}
}

export type DestinosViajesAction = NuevoDestinoAction | ElegidoFavoritoAction | VoteUpAction | VoteDownAction;

export function reducerDestinosViajes(
  state: DestinosViajesState,
  action: DestinosViajesAction
): DestinosViajesState {
  switch (action.type) {
    case DestinosViajesActionTypes.nuevoDestino: {
      return {
        ...state,
        items: [...state.items, (action as NuevoDestinoAction).destino]
      };
    }
    case DestinosViajesActionTypes.voteUp: {
      const d: DestinoViaje = (action as VoteUpAction).destino;
      d.VoteUp();
      return {
        ...state
      };
    }
    case DestinosViajesActionTypes.voteUp: {
      const d: DestinoViaje = (action as VoteDownAction).destino;
      d.VoteDown();
      return {
        ...state
      };
    }
  }
  return state;
}

@Injectable()
export class DestinoViajesEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(DestinosViajesActionTypes.nuevoDestino),
    map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
  );
  constructor(private actions$: Actions) {}
}
