import {v4 as uuid} from 'uuid';

export class DestinoViaje {
  selected: boolean;
  servicios: string[];
  id = uuid();

  constructor(public nombre: string, public imagenUrl: string, public votes: number = 0) {
    this.servicios = ['Pileta', 'Desayuno', 'Gimnasio'];
  }
  isSelected(): boolean {
    return this.selected;
  }
  setSelected(s: boolean) {
    this.selected = s;
  }
  VoteUp() {
    this.votes ++;
  }
  VoteDown() {
    this.votes --;
  }
}
